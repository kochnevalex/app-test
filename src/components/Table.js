import React from 'react';
//UI
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

function Table({ grid, gridTable, history, setHistory }) {
  const mouseOver = (e, el) => {
    e.target.style.backgroundColor = '#0000ff';
    e.target.style.color = '#fff';
  }
  const mouseOut = (e, el) => {
    e.target.style.backgroundColor = '#fff'
    e.target.style.color = '#000';
    setHistory(history => [el, ...history])
  }
  const classes = useStyles();
  return (
    <Grid container justify="space-between" >
      {gridTable.map((item, index) => (
        <Grid container key={index}>
          <React.Fragment>
            {item.map((el, elIndex) => (
              <Grid key={elIndex} item xs>
                <Paper square onMouseOver={(e) => mouseOver(e, el)} onMouseOut={(e) => mouseOut(e, el)} className={classes.paper}>
                </Paper>
              </Grid>
            ))}
          </React.Fragment>
        </Grid>
      ))}
    </Grid>
  )
}
export default Table;

const useStyles = makeStyles((theme) => ({
  paper: {
    paddingBottom: '100%',
    boxShadow: 'none',
    outline: '1px solid #000'
  }
}));