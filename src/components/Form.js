//UI
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';


function Form({ mode, grid, setGrid, start, setStart, gridTable, setGridTable, setHistory }) {
  const putMode = (e) => {
    let gridSize = e.target.value;
    setGrid(gridSize)
    let gridArr = []
    for (let row = 1; row <= gridSize; row++) {
      let gridRow = [];
      for (let col = 1; col <= gridSize; col++) {
        let gridItem = { id: `${row}${col}`, row: row, col: col, active: false }
        gridRow.push(gridItem)
      }
      gridArr.push(gridRow)
    }
    setGridTable(gridArr)
    setHistory([])
  }
  const startGrid = (e) => {
    e.preventDefault();
    setStart(!start);
    setHistory([]);
  }
  const classes = useStyles();

  return (
    <form action="" className={classes.form}>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <Select
            value={grid}
            onChange={putMode}
            className={classes.select}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
          >
            <MenuItem value='' disabled selected>
              Pick mode
          </MenuItem>
            {mode && mode.map((item, index) => (<MenuItem key={index} value={parseInt(item.field)}>{item.name}</MenuItem>))}
          </Select>
        </Grid>
        <Grid item xs={4}>
          <Button onClick={startGrid} disabled={grid ? false : true} variant="contained" color={start ? 'secondary' : 'primary'} className={classes.button}>
            {start ? 'Stop' : 'Start'}
          </Button>
        </Grid>
      </Grid>

    </form >
  )

}

export default Form;

const useStyles = makeStyles((theme) => ({
  form: {
    paddingBottom: '30px'
  },
  button: {
    width: '100%'
  },
  select: {
    width: '100%'
  }
}));