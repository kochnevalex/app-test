//UI
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

function History({ history }) {
  const classes = useStyles();
  return (
    <div>
      <Typography variant="h5" className={classes.title} >History</Typography>
      <List component="nav" className={classes.list}>
        {history.map((el, index) => (
          <ListItem key={index} className="list-item" className={classes.listItem}>
            <ListItemText className={classes.listItemText}>
              row {el.row} col {el.col}
            </ListItemText>
          </ListItem>
        ))}
      </List>
    </div>
  )
}

export default History;

const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: 'center',
    marginBottom: '20px'
  },
  list: {
    maxHeight: '420px',
    overflowY: 'auto',
  },
  listItem: {
    padding: '0',
  },
  listItemText: {
    textAlign: 'center',
    fontSize: '12px',
    lineHeight: '15px',
    padding: '5px 10px',
    background: '#fbf8e5',
    border: '1px solid #f8edd4',
    borderRadius: '3px',
    color: '#856e42'
  }
}));