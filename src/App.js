import { useState, useEffect } from 'react';
import axios from 'axios';
//UI
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
//components
import Form from './components/Form';
import Table from './components/Table';
import History from './components/History';


function App() {
  const [mode, setMode] = useState();
  const [grid, setGrid] = useState('');
  const [gridTable, setGridTable] = useState([]);
  const [start, setStart] = useState(false);
  const [history, setHistory] = useState([]);

  useEffect(() => {
    const getData = async () => {
      await axios.get('http://demo1030918.mockable.io/').then((res) => {
        const res_mode = [];
        for (const [key, value] of Object.entries(res.data)) {
          res_mode.push({ name: key, field: value.field })
        }
        setMode(res_mode)
      })
    }
    getData();
  },[])

  const classes = useStyles();

  return (
    <Container maxWidth="sm" className={classes.container}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={9} >
          <Form mode={mode} setGrid={setGrid} grid={grid} setStart={setStart} start={start} gridTable={gridTable} setGridTable={setGridTable} setHistory={setHistory} />
          {start && <Table grid={grid} gridTable={gridTable} setGridTable={setGridTable} setHistory={setHistory} history={history} />}
        </Grid>
        <Grid item xs={12} sm={3} >
          <History history={history} />
        </Grid>
      </Grid>
    </Container>
  );
}
export default App;

const useStyles = makeStyles((theme) => ({
  container: {
    padding: '50px 16px',

  }
}));